from django.db import models
from django.utils import timezone

# Create your models here.

class Activity(models.Model):
    title = models.CharField(max_length = 50, null = True)
    desc = models.CharField(max_length = 300, null = True)
    date = models.DateTimeField(default=timezone.now)

class Subscriber(models.Model):
    nama_pendaftar = models.CharField(max_length = 50)
    email_pendaftar = models.CharField(max_length = 200)
    password_pendaftar = models.CharField(max_length = 32)
