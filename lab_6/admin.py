from django.contrib import admin
from .models import Activity, Subscriber

admin.site.register(Activity)
admin.site.register(Subscriber)
# Register your models here.
