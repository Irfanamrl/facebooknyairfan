from django.shortcuts import render, redirect
import requests
from django.http import HttpResponse, JsonResponse
from .models import Activity, Subscriber
from .forms import Add_Activity, Subscriber_Form
from django.core.validators import validate_email
from django.core import serializers
from django.contrib.auth import logout
from django.views.decorators.csrf import csrf_exempt

def home(request):
    form = Add_Activity(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        status = Activity()
        status.title = form.cleaned_data['title']
        status.desc = form.cleaned_data['desc']
        status.save()
        html = 'index.html'
        return redirect('lab_6:home')

    else:
        activity = Activity.objects.all()
        form = Add_Activity()
        response = {'form' : form, 'activity': activity}
        return render(request, 'index.html', response)

def profile(request):
     return render(request, 'profile.html')


def get_json(request, variable='quilting'):
    raw_data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + variable).json()
    items_ =[]
    print(raw_data)
    for info in raw_data['items']:
        data = {}
        data['image'] = info['volumeInfo']['imageLinks']['thumbnail']
        data['title'] = info['volumeInfo']['title']
        if ('authors' in info['volumeInfo']):
            data['author'] = " and ".join(info['volumeInfo']['authors'])
        else:
            data['author'] = '-'
        if ('publishedDate' in info['volumeInfo']):
            data['publishedDate'] = info['volumeInfo']["publishedDate"]
        else:
            data['publishedDate'] = "Unknown"
        items_.append(data)
    return JsonResponse({"data" :items_})

def subscriber_add(request):
    if request.method == 'POST':
        print('masuk kesini ga sih?')
        subscribe = Subscriber(
            nama_pendaftar = request.POST['name'],
            email_pendaftar = request.POST['email'],
            password_pendaftar = request.POST['password']
        )
        subscribe.save()
        return JsonResponse({
            'message':'Subscribe Success!'
        }, status = 200)
    else:
        return JsonResponse({
            'message':"There's no GET method here"
        }, status = 403)

def login_page(request):
    response={}
    response['form'] = Subscriber_Form
    return render(request, 'login.html', response)

def check_email(request):
    print('stdouhghj')
    try:
        print('masuk try')
        print(request.POST["email"])
        validate_email(request.POST["email"])
    except:
        print('masuk except')
        return JsonResponse({
            'message':'Email format is wrong!',
            'status':'fail'
        })

    exist = Subscriber.objects.filter(email_pendaftar=request.POST['email'])

    if exist:
        return JsonResponse({
            'message':'Email already exist',
            'status':'fail'
        })

    return JsonResponse({
        'message':'Email can be used',
        'status':'success'
    })

def get_subscriber(request):
    if request.method == 'GET':
        print("masuk ke get_subsciber")
        lst_subscriber = serializers.serialize('json', Subscriber.objects.all())
        response = lst_subscriber

    else:
        response['message'] = 'Method POST not ALLOWED HERE'
    return HttpResponse(response, content_type='application/json')

def del_subscriber(request):
    if request.method == 'POST':
        print("masuk ke del_subscriber")
        theOne = Subscriber.objects.get(pk=request.POST.get('pk'))
        print(theOne)
        theOne.delete()
        response['message'] = "success"
    else:
        response['message'] = "Method GET not allowed here"
    return JsonResponse(response)

def books(request):
    if request.user.is_authenticated :
        if "like" not in request.session:
            request.session["fullname"]=request.user.first_name+" "+request.user.last_name
            request.session["username"]=request.user.username
            request.session["email"] = request.user.email
            request.session["sessionid"]=request.session.session_key
            request.session["like"]=[]

    return render(request, 'books.html')

def choice(request):
    return render(request, 'choice.html')

def logingOut(request):
    logout(request)
    return redirect('lab_6:books')

@csrf_exempt
def like(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] not in lst :
            lst.append(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")

@csrf_exempt
def unlike(request):
    if(request.method=="POST"):
        lst = request.session["like"]
        if request.POST["id"] in lst :
            lst.remove(request.POST["id"])
        print(request.session["sessionid"])
        print(request.session["like"])
        request.session["like"]= lst
        response["message"]=len(lst)
        return JsonResponse(response)
    else:
        return HttpResponse("GET Method not allowed")


def get_like(request):
    if request.user.is_authenticated :
        if(request.method=="GET"):
            if request.session["like"] is not None:
                response["message"] = request.session["like"]
        else:
            response["message"] = "NOT ALLOWED"
    else:
        response["message"] = ""
    return JsonResponse(response)
