var count = 0;
function mencari(variable){
  $.ajax({
    type : 'GET',
    url: "/lab_6/get_json/variable=" + variable,
    dataType: 'json',
    success: function(data){
            $('tbody').empty();
            var temp = '';
            console.log("ea")
            for(var i = 0;i<data.data.length;i++){
              temp+='<tr class="text-center"><th scope="row">' + (i+1) + '</th>';
              temp+='<td class="align-middle"> <img class="img-responsive" src="' + data.data[i].image+'"></td>';
              temp+='<td class="align-middle"><b>' + data.data[i].title+'</b></td>';
              temp+='<td class="align-middle">'+ data.data[i].author +'</td>';
              temp+='<td class="align-middle">'+ data.data[i].publishedDate +'</td>';
              temp+='<td class="align-middle"><i onclick="clicked(star'+i+')" class="fas fa-star" id="star'+i+'" style="font-size:30px;"></i></td>';
              temp+='</tr>';
            }
            $('tbody').append(temp);

        }
  });
};

function clicked(i){
  if( $(i).hasClass('clicked') ) {
    count -=1;
    console.log(count);
    $(i).removeClass('clicked');
    $(i).css("color","#000000");
  } else {
    count +=1;
    console.log(count);
    $(i).addClass('clicked');
    $(i).css("color","#FFD300");
  }
  $('.count').html(count);
}
$(document).ready(function(){
  mencari('quilting');
  $(window).load(function() {
  		// Animate loader off screen
      setTimeout(function(){ $(".se-pre-con").fadeOut("slow"); }, 2000);
  });

  var clicked = false;
  $("#button").click(function() {
    if (clicked == false) {
      $(".profilePage").css("background-color", "#000000");
      $("h1").css("color", "#FFFFFF");
      $("p.profileDesc").css("color", "#FFFFFF");
      clicked = true;
    }
    else {
      $(".profilePage").css("background-color", "#FFFFFF");
      $("h1").css("color", "#000000");
      $("p.profileDesc").css("color", "#000000");
      clicked = false;
    }
  });
});

function search() {
    var word = $('input[name="pencarian"]').val();
    mencari(word);
}

var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });

    $("#emailForm").keyup(function() {
        checkEmail();
    });

    $("#passwordForm").keyup(function() {
        $('#statusForm').html('');
        if ($('#passwordForm').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });


    $('#submit').click(function() {
        var data = {
            'email' : $('#emailForm').val(),
            'name' : $('#nameForm').val(),
            'password' : $('#passwordForm').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        console.log(data)
        $.ajax({
            type : 'POST',
            url : 'subscriber_add',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('emailForm').value = '';
                document.getElementById('nameForm').value = '';
                document.getElementById('passwordForm').value = '';
                $('#statusForm').html('');
                checkAll();
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: 'check_email',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }

        }
    });
}

function checkAll() {
    if (emailIsValid &&
        $('#nameForm').val() !== '' &&
        $('#passwordForm').val() !== '' &&
        $('#passwordForm').val().length > 7) {

        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}

function deleteSubscriber(id){
    $.ajax({
        url: "del_subscriber",
        type: "POST",
        data: {
            pk: id,
            csrfmiddlewaretoken : document.getElementsByName('csrfmiddlewaretoken')[0].value
        },
        success: function(result) {
            console.log($(this));
             $('#listSubscriber').html("");
            getSubscriber();
           alert("Unsubscribed");
        },
        error : function (errmsg){
            alert("Something is wrong");
        }
    });
};

function getSubscriber(){
     $.ajax({
        url: "get_subscriber",
        type: "GET",
        success: function(result){
            console.log(result[0]);
            for(i=0; i< result.length;i++){
                $('#listSubscriber').append("<div class='row'style='padding-bottom:10px; padding-top:10px;'><div class='col-sm-4'>"+result[i]['field']+"</div>"+
                "<div class='col-sm-4'>"+result[i]['field']+"</div><div class='col-sm-4'><button onclick='deleteSubscriber("+
                result[i]['pk']+")' class='btn btn-danger'"+">Unsubscribe</button></div></div>");
            }

        },
        error: function(errmsg){
            alert("errmsg");
        }
    });
};

$(document).ready(function(){
   getSubscriber();
});
