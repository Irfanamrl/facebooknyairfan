from django import forms
from .models import Activity, Subscriber

class Add_Activity(forms.Form):
    error_messages = {
    'required': 'Please type',
    }

    title_attrs = {
    'type' : 'text',
    'class': "form-group col-md-12",
    'placeholder': 'Masukin nama',
    'id' : 'Id_title'
    }

    description_attrs = {
    'type': 'text',
    'class': "form-group col-md-12",
    'placeholder': 'Apa yang sedang kamu pikirkan?',
    'id' : 'id_description'
    }

    title = forms.CharField(label='Nama', required=True, max_length=27, widget=forms.TextInput(attrs=title_attrs))
    desc = forms.CharField(label='Status', required=True, max_length = 200, widget=forms.TextInput(attrs=description_attrs))

class Subscriber_Form(forms.ModelForm):
    class Meta:
        model = Subscriber
        fields = ['nama_pendaftar', 'email_pendaftar', 'password_pendaftar']

    nama_pendaftar = forms.CharField(label='Nama', required=True, max_length = 50, widget=forms.TextInput(attrs={'class':'form-control', 'id':'nameForm'}))
    email_pendaftar = forms.CharField(label='Email', required=True, max_length = 200, widget=forms.TextInput(attrs={'class':'form-control', 'type':'email', 'id':'emailForm'}))
    password_pendaftar = forms.CharField(label='Password', max_length=32, widget=forms.PasswordInput(attrs={'class':'form-control', 'id':'passwordForm'}))

    def clean(self):
        cleaned_data = super(Subscriber_Form, self).clean()
        email_dicari = cleaned_data.get('email_pendaftar')
        email_in_db = Subscriber.objects.filter(email_pendaftar=email_dicari)

        if email_in_db.exists():
            self.add_error('email_pendaftar', 'This email has been taken.')
