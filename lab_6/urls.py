from .views import *
from django.urls import path, include, re_path
from django.contrib.auth import views
from django.conf.urls import url

app_name='lab_6'
urlpatterns = [
    path('', home, name='home'),
    path('profile', profile, name='profile'),
    path('get_json/variable=<str:variable>', get_json, name='get_json'),
    path('books', books, name='books'),
    path('login_page', login_page, name='login_page'),
    path('check_email', check_email, name='check_email'),
    path('subscriber_add', subscriber_add, name='subscriber_add'),
    path('get_subscriber', get_subscriber, name="get_subscriber"),
    path('del_subscriber', del_subscriber, name='del_subscriber'),
    path('choice', choice, name='choice'),
    path('logingOut', logingOut, name='logingOut'),
    path('like', like, name = 'like'),
    path('get_like', get_like, name ='get_like'),
    path('unlike', unlike, name = 'unlike')
]
