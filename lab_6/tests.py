from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home, profile
from .models import Activity
from .forms import Add_Activity
from .apps import Lab6Config
from django.apps import apps
from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.webdriver.chrome.options import Options


# Create your tests here.


class Lab6UnitTest(TestCase):

    def test_lab6_url_does_exist(self):
        response = Client().get('/lab_6/')
        self.assertEqual(response.status_code, 200)

    def test_lab6_using_index_func(self):
        found = resolve('/lab_6/')
        self.assertEqual(found.func, home)

    def test_model_can_create_new_activity(self):
        # Creating a new activity
        new_activity = Activity.objects.create(title='mengerjakan lab ppw',
        desc ='mengerjakan lab_6 ppw')

        counting_all_available_activity = Activity.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    def test_form_todo_input_has_placeholder_and_css_classes(self):
        form = Add_Activity()
        self.assertIn('class="form-group col-md-12"', form.as_p())
        self.assertIn('id="Id_title"', form.as_p())
        self.assertIn('class="form-group col-md-12"', form.as_p())
        self.assertIn('id="id_description"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Add_Activity(data={'title': '', 'desc': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['desc'], ["This field is required."])

    def test_lab6_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab_6/', {'title': test, 'desc': test})
        self.assertEqual(response_post.status_code, 302)

        response = Client().get('/lab_6/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_apps(self):
        self.assertEqual(Lab6Config.name, 'lab_6')
        self.assertEqual(apps.get_app_config('lab_6').name, 'lab_6')

    def test_lab6_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/lab_6/', {'title': '', 'desc': ''})
        self.assertNotEqual(response_post.status_code, 302)

        response = Client().get('/lab_6/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    def test_lab6_profile_url_does_exist(self):
        response = Client().get('/lab_6/profile')
        self.assertEqual(response.status_code, 200)

    def test_lab6_contains_name(self):
        name = 'Muhammad Irfan Amrullah'
        response = Client().get('/lab_6/profile')
        html_response = response.content.decode('utf8')
        self.assertIn(name, html_response)

    def test_lab6_contains_alamat(self):
        alamat = 'Jl Pancoran Barat IX G'
        response = Client().get('/lab_6/profile')
        html_response = response.content.decode('utf8')
        self.assertIn(alamat, html_response)

class Lab6FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(Lab6FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        #find the form element
        title = selenium.find_element_by_id('Id_title')
        description = selenium.find_element_by_id('id_description')
        submit = selenium.find_element_by_id('submit')

        title.send_keys('Irfan')
        description.send_keys('saya harap ini langsung bisa')

        submit.send_keys(Keys.RETURN)

        page = selenium.page_source
        self.assertIn("saya harap ini langsung bisa", page)

    def test_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        self.assertIn("Home", selenium.title)

    def test_jumbotron(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        page = selenium.find_element_by_tag_name('h1')
        self.assertIn('Hello, Apa Kabar?', page.text)

    def test_css_style_blue(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        temp = selenium.find_element_by_tag_name('h1')
        warna = temp.value_of_css_property('color')
        self.assertIn('rgba(135, 206, 250, 1)', warna)

    def test_css_style_red(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000')
        temp = selenium.find_element_by_tag_name('p')
        warna = temp.value_of_css_property('color')
        self.assertIn('rgba(204, 0, 0, 1)', warna)

    def test_change_body_color(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/lab_6/profile')
        time.sleep(5)
        #find the form element
        submit = selenium.find_element_by_id('button')
        temp1 = selenium.find_element_by_class_name('profilePage')
        warna1 = temp1.value_of_css_property('background-color')

        time.sleep(5)
        submit.click()

        temp2 = selenium.find_element_by_class_name('profilePage')
        warna2 = temp2.value_of_css_property('background-color')
        self.assertNotEqual(warna2, warna1)
