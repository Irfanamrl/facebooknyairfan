var count = 0;
$(document).ready(function(){
  $.ajax({
    type : 'GET',
    url: "/lab_6/get_json",
    dataType: 'json',
    success: function(data){
    var temp = '';
    console.log("ea")
    for(var i = 0;i<data.data.length;i++){
      temp+='<tr class="text-center"><th scope="row">' + (i+1) + '</th>';
      temp+='<td class="align-middle"> <img class="img-responsive" src="' + data.data[i].image+'"></td>';
      temp+='<td class="align-middle"><b>' + data.data[i].title+'</b></td>';
      temp+='<td class="align-middle">'+ data.data[i].author +'</td>';
      temp+='<td class="align-middle">'+ data.data[i].publishedDate +'</td>';
      temp+='<td class="align-middle"><i class="fas fa-star" id="star" style="font-size:30px;"></i></td>';
      temp+='</tr>';
    }
    $('tbody').append(temp);
  }
});

$(document).ready(function(){
  $(window).load(function() {
  		// Animate loader off screen
      setTimeout(function(){ $(".se-pre-con").fadeOut("slow"); }, 2000);
  });

  var clicked = false;
  $("#button").click(function() {
    if (clicked == false) {
      $(".profilePage").css("background-color", "#000000");
      $("h1").css("color", "#FFFFFF");
      $("p.profileDesc").css("color", "#FFFFFF");
      clicked = true;
    }
    else {
      $(".profilePage").css("background-color", "#FFFFFF");
      $("h1").css("color", "#000000");
      $("p.profileDesc").css("color", "#000000");
      clicked = false;
    }
  });

  $("#star").click(function() {
    if( $(this).hasClass('clicked') ) {
      count -=1;
      console.log(count);
      $(this).removeClass('clicked');
      $(this).css("color","rgba(150,150,150,0.5);");
    } else {
      count +=1;
      console.log(count);
      $(this).addClass('clicked');
      $(this).css("color","#FFD300");
    }
    $.('.count').html(counter);
  });
});
