$(document).ready(function(){
  var clicked = false;
  $("#button").click(function() {
    if (clicked == false) {
      $(".profilePage").css("background-color", "#000000");
      $("h1").css("color", "#FFFFFF");
      $("p").css("color", "#FFFFFF");
      clicked = true;
    }
    else {
      $(".profilePage").css("background-color", "#FFFFFF");
      $("h1").css("color", "#000000");
      $("p").css("color", "#000000");
      clicked = false;
    }
  });
});
